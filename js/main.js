const SPADE = "spade";
const CLUB = "club";
const DIAMOND = "diamond";
const HEART = "heart";

const RESULT_INPUT = document.getElementById("blackjack-result");

const SUIT = [SPADE, CLUB, DIAMOND, HEART];
const CARD_VALUE = [
  "2",
  "3",
  "4",
  "5",
  "6",
  "7",
  "8",
  "9",
  "10",
  "J",
  "Q",
  "K",
  "A",
];

function checkSuit(value) {
  switch (value) {
    case 1:
      return SPADE;
    case 2:
      return CLUB;
    case 3:
      return DIAMOND;
    case 4:
      return HEART;
  }
}

// số điểm
let dealerCards = [];
let dealerScore = 0;

let playerCards = [];
let playerScore = 0;

// Điểm từng lá bài
function getCardValue(value, scoreOfWhom) {
  switch (value) {
    default:
      return value * 1;
    case "J":
    case "Q":
    case "K":
      return 10;
    case "A":
      if (scoreOfWhom <= 10) return 11;
      else if (scoreOfWhom == 11) return 10;
      else if (scoreOfWhom > 11) return 1;
  }
}

// Tạo 1 card random
function randomCard(toWhom, numberID, suitID) {
  var value = CARD_VALUE[Math.floor(Math.random() * CARD_VALUE.length)];
  var suit = SUIT[Math.floor(Math.random() * SUIT.length)];

  if (numberID == null && suitID == null) {
    toWhom.push({ value, suit });
    return { value, suit };
  }

  document.getElementById(numberID).innerHTML = value;
  document.getElementById(suitID).src = `./image/${suit}.png`;

  if (suit == DIAMOND || suit == HEART) {
    document.getElementById(numberID).style.color = "red";
  } else {
    document.getElementById(numberID).style.color = "black";
  }
  toWhom.push({ value, suit });
  return { value, suit };
}

function checkBJ(ofWhom) {
  // Check BLACKJACK
  if (ofWhom[0].value == "A") {
    switch (ofWhom[1].value) {
      case "J":
      case "Q":
      case "K":
      case "A":
        return "BJ";
      case "10":
        return 21;
      default:
        break;
    }
  } else if (ofWhom[1].value == "A") {
    switch (ofWhom[0].value) {
      case "J":
      case "Q":
      case "K":
      case "A":
        return "BJ";
      case "10":
        return 21;
      default:
        break;
    }
  }
  return false;
}

// Hiển thị lá bài lên HTML
function showCard(number) {
  document.getElementById(`card-back-${number}`).classList.add("d-none");
  document.getElementById(`card-front-${number}`).classList.remove("d-none");
}

// Thêm bài cho người chơi
function addPlayerCard() {
  let card = randomCard(playerCards);
  return `
    <div class="player_card">
        <span
            class="card-number"
            style="color:${
              card.suit == DIAMOND || card.suit == HEART ? "red" : "black"
            }"
        >${card.value}</span>
        <img
            class="card-suit"
            src="./image/${card.suit}.png"
        />
    </div>
  `;
}

let playerCardHTML = "";
function addCard() {
  playerCardHTML += addPlayerCard(playerCards);
  document.getElementById("player-cards").innerHTML = playerCardHTML;

  getPlayerScore();
  if (playerScore > 21) {
    RESULT_INPUT.innerHTML = "YOU'RE BUSTED!";
    showModal();
  }
}

// Thêm bài cho dealer
function addDealerCard() {
  let card = randomCard(dealerCards);
  document.getElementById("dealer-cards").innerHTML += `
    <div
              class="dealer_card border border-3 border-dark py-3 px-3 w-100 text-center"
            >
              <div class="card_front">
                <span id="card2-number" style="color:${
                  card.suit == DIAMOND || card.suit == HEART ? "red" : "black"
                }" class="card-number">${card.value}</span>
                <br />
                <img
                  id="card2-suit"
                  class="card-suit"
                  src="./image/${card.suit}.png"
                />
              </div>`;
}

function startGame() {
  document.getElementById("player-section").classList.remove("d-none");
  document.getElementById("startBTN").disabled = true;
  playerCardHTML = "";
  showCard(1);
  randomCard(dealerCards, "card1-number", "card1-suit");
  randomCard(dealerCards, "card2-number", "card2-suit");

  playerCardHTML = addPlayerCard() + addPlayerCard();
  document.getElementById("player-cards").innerHTML = playerCardHTML;

  if (checkBJ(playerCards)) {
    RESULT_INPUT.innerHTML = "BLACKJACK";
    showModal();
  }
}

// Tính điểm cho người chơi
function getPlayerScore() {
  playerScore = 0;
  let aceCardsCount = 0;
  for (var i = 0; i < playerCards.length; i++) {
    if (playerCards[i].value == "A") {
      aceCardsCount++;
    } else {
      playerScore += getCardValue(playerCards[i].value, playerScore);
    }
  }

  for (var i = 0; i < aceCardsCount; i++) {
    playerScore += getCardValue("A", playerScore);
  }
}

// Tính điểm cho dealer
function getDealerScore() {
  dealerScore = 0;
  let aceCardsCount = 0;
  for (var i = 0; i < dealerCards.length; i++) {
    if (dealerCards[i].value == "A") {
      aceCardsCount++;
    } else {
      dealerScore += getCardValue(dealerCards[i].value, dealerScore);
    }
  }

  for (var i = 0; i < aceCardsCount; i++) {
    dealerScore += getCardValue("A", dealerScore);
  }
}

// Tính toán kết quả
function calResult() {
  if (playerScore <= 22 && dealerScore > 21) {
    RESULT_INPUT.innerHTML = "YOU WIN!";
    return;
  }

  if (playerScore > 21) {
    RESULT_INPUT.innerHTML = "YOU'RE BUSTED!";
  } else if (playerScore == dealerScore) {
    RESULT_INPUT.innerHTML = "DRAW";
  } else if (playerScore < dealerScore) {
    RESULT_INPUT.innerHTML = "YOU LOSE!";
  } else if (playerScore > dealerScore) {
    RESULT_INPUT.innerHTML = "YOU WIN!";
  }
}

// Open Modal
function showModal() {
  var myModal = new bootstrap.Modal(document.getElementById("resultModal"), {
    keyboard: false,
  });
  myModal.show();
}
// Show Kết quả
let count = 0;
function showResult() {
  showCard(2);
  getPlayerScore();

  if (checkBJ(dealerCards) == "BJ") {
    RESULT_INPUT.innerHTML = "YOU LOSE!";
    showModal();
    return;
  } else if (checkBJ(dealerCards) == 21) {
    dealerScore = 21;
    calResult();
    showModal;
    return;
  } else getDealerScore();

  if (dealerScore < 17) {
    do {
      addDealerCard();
      getDealerScore();
      if (dealerScore == 17) {
        break;
      }
    } while (dealerScore < 17);
  }

  console.log(count);
  calResult();
  showModal();
  console.log("dealerScore: ", dealerScore);
  console.log("playerScore: ", playerScore);

  return;
}

// Load lai trang
function playAgain() {
  window.location.reload();
}
